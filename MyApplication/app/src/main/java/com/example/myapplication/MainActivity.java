package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        
        //Configuracion de la base de datos
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase baseDeDatos = admin.getWritableDatabase();
        Context context = getApplicationContext();

        //Verificamos datos en bd
        Cursor query = baseDeDatos.rawQuery("select * from Tareas",null);
        boolean rowBoolean = query.moveToFirst();
        while(rowBoolean) {
            addItemView(context,baseDeDatos,query.getString(1), query.getInt(2));
            rowBoolean = query.moveToNext();
        }

        //Funcionamiento Ingresar texto mediante botón
        EditText inputTextView = findViewById(R.id.editTextTextPersonName19);
        inputTextView.setText("Inserte Titulo");
        inputTextView.setOnClickListener(view -> inputTextView.setText(""));

        Button inputButton = findViewById(R.id.inputButton);
        inputButton.setOnClickListener(view -> addItemFromInput(context, view, baseDeDatos));

    }

    //Metodo para crear Tareas a traves de teclado.
    public void addItemFromInput(Context context, View view,SQLiteDatabase db){
        EditText inputTextView = findViewById(R.id.editTextTextPersonName19);
        CheckBox checbox = findViewById( R.id.checkBox2 );
        int check = checbox.isChecked()? 1 : 0;
        String inputText = inputTextView.getText().toString();
        Cursor verify = db.rawQuery("select * from Tareas where titulo="+"'"+inputText+"'",null);
        if (verify != null){
            boolean ver = verify.moveToFirst();
            if(!(ver)){
                if (!(inputText.equals(""))){
                    addItem(context,db,inputText, check);
                    inputTextView.setText("");
                }
            }else{
                Toast.makeText(context,"Ya has añadido esta tarea!",Toast.LENGTH_LONG).show();
            }
        }
    }

    //Metodo para crear vistas en la aplicacion por cada tarea.
    public void addItemView(Context context,SQLiteDatabase db, String titulo,int check){
        TableRow.LayoutParams param = new TableRow.LayoutParams( TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT,2.0f);
        TableRow.LayoutParams param2 = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT,3.0f);

        //Creacion de elementos para cada tarea.
        TextView txt = new TextView(context);
        txt.setText(titulo);
        txt.setGravity(Gravity.CENTER);
        txt.setTextColor(Color.BLACK);
        Button but = new Button(context);
        but.setText("Eliminar");

        TableRow tableRow = new TableRow(context);
        tableRow.addView(txt,param);
        tableRow.addView(but,param2);

        LinearLayout linearLayout = findViewById(R.id.LinearLayoutW);
        linearLayout.addView(tableRow);

        //Si esta tarea es importante, se enmarca con fondo rojo.
        if (check == 1){tableRow.setBackgroundColor(Color.RED);}

        //Se implementa la funcionalidad de que las tareas puedan variar entre importantes o no.
        tableRow.setOnLongClickListener((View e)-> {
            Cursor cursor = db.rawQuery("select * from tareas where titulo="+"'"+titulo+"'", null);
            cursor.moveToFirst();
            if (cursor.getInt(2) == 1){
                db.rawQuery("update tareas set destacada = 0 where titulo="+"'"+titulo+"'", null);
                ContentValues update1 = new ContentValues();
                update1.put("id",cursor.getInt(0));
                update1.put("titulo", titulo);
                update1.put("destacada",0);
                String whereclause = "titulo='"+titulo+"'";
                db.update("Tareas",update1,whereclause,null);
                tableRow.setBackgroundColor(Color.WHITE);
            }else{
                db.rawQuery("update tareas set destacada = 0 where titulo="+"'"+titulo+"'", null);
                ContentValues update1 = new ContentValues();
                update1.put("id",cursor.getInt(0));
                update1.put("titulo", titulo);
                update1.put("destacada",1);
                String whereclause = "titulo='"+titulo+"'";
                db.update("Tareas",update1,whereclause,null);
                tableRow.setBackgroundColor(Color.RED);
            }
            return true;
        });

        //Se implementa el boton para eliminar la tarea de la base de datos y su respectiva vista.
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearLayout.removeView(tableRow);
                String whereClause = "titulo="+"'"+titulo+"'";
                db.delete("Tareas",whereClause,null);
                Toast.makeText(context, "Tarea eliminada!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Metodo para generar crear Tareas tanto en la base de datos como visualmente.
    public void addItem(Context context,SQLiteDatabase db,String titulo,int check){
        //Insercion en Base de Datos
        ContentValues tarea = new ContentValues();
        tarea.put("titulo", titulo);
        tarea.put("destacada",check);
        db.insert("Tareas", null, tarea);

        //Se llama a la creacion de la vista
        addItemView(context,db,titulo, check);
    }
}