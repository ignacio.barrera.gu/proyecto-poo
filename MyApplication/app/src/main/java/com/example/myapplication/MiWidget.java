package com.example.myapplication;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.RemoteViews;

public class MiWidget extends AppWidgetProvider{

    public void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int n = appWidgetIds.length;

        //Se genera la conexion a la base de datos.
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context, "administracion", null, 1);
        SQLiteDatabase baseDeDatos = admin.getWritableDatabase();

        //Se crean las vistas de las tareas importantes dentro del widget.
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.mi_widget);
        for (int i = 0; i < n; i++){
            Cursor query = baseDeDatos.rawQuery("select * from Tareas where Tareas.destacada = 1",null);
            boolean rowBoolean = query.moveToFirst();
            int j = 0;
            while(rowBoolean && j < 4){
                switch (j){
                    case 0:
                        views.setTextViewText(R.id.tv0, query.getString(1));
                        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds[i], R.id.tv0);
                        break;
                    case 1:
                        views.setTextViewText(R.id.tv1, query.getString(1));
                        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds[i], R.id.tv1);
                        break;
                    case 2:
                        views.setTextViewText(R.id.tv2, query.getString(1));
                        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds[i], R.id.tv2);
                        break;
                    case 3:
                        views.setTextViewText(R.id.tv3, query.getString(1));
                        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds[i], R.id.tv3);
                        break;
                }
                rowBoolean = query.moveToNext();
                j++;
            }
        }
        appWidgetManager.updateAppWidget(appWidgetIds, views);
    }

   @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds){
        for (int appWidget : appWidgetIds){
            updateAppWidget(context, appWidgetManager, appWidgetIds);
        }
   }

}